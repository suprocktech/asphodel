// AUTOGENERATED FILE. DO NOT MODIFY.

#ifndef UNPACK_H_
#define UNPACK_H_


#include <stdint.h>


#ifdef __cplusplus
extern "C" {
#endif


// cross platform definitions for shared/static library support
// see https://gcc.gnu.org/wiki/Visibility
#if defined _WIN32 || defined __CYGWIN__
#  define UNPACK_HELPER_DLL_IMPORT __declspec(dllimport)
#  define UNPACK_HELPER_DLL_EXPORT __declspec(dllexport)
#  define UNPACK_HELPER_DLL_LOCAL
#else
#  if __GNUC__ >= 4
#    define UNPACK_HELPER_DLL_IMPORT __attribute__ ((visibility ("default")))
#    define UNPACK_HELPER_DLL_EXPORT __attribute__ ((visibility ("default")))
#    define UNPACK_HELPER_DLL_LOCAL  __attribute__ ((visibility ("hidden")))
#  else
#    define UNPACK_HELPER_DLL_IMPORT
#    define UNPACK_HELPER_DLL_EXPORT
#    define UNPACK_HELPER_DLL_LOCAL
#  endif
#endif

#ifdef UNPACK_STATIC_LIB // defined if unpack library is compiled as static lib
#  define UNPACK_API
#  define UNPACK_LOCAL
#else // UNPACK_STATIC_LIB is not defined: this means unpack is a DLL
#  ifdef UNPACK_API_EXPORTS // defined if we are building the library (instead of using it)
#    define UNPACK_API UNPACK_HELPER_DLL_EXPORT
#  else
#    define UNPACK_API UNPACK_HELPER_DLL_IMPORT
#  endif // UNPACK_API_EXPORTS
#  define UNPACK_LOCAL UNPACK_HELPER_DLL_LOCAL
#endif // UNPACK_STATIC_LIB


typedef void (*unpack_func_t)(const uint8_t *input, double *output, void *closure);
typedef uint64_t (*unwrap_func_t)(const uint8_t *input, uint64_t last);
typedef uint8_t (*unpack_id_func_t)(const uint8_t *input);

UNPACK_API unpack_func_t find_unpack(int count, int bits, int is_signed, int offset, void **closure);
UNPACK_API unwrap_func_t find_unwrap(int bits, int offset);
UNPACK_API unpack_id_func_t find_unpack_id(int bits, int offset);

#define free_unpack(func, closure) do { (void)(func); (void)(closure); } while (0)


#ifdef __cplusplus
}
#endif


#endif /* UNPACK_H_ */
