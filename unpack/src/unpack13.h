// AUTOGENERATED FILE. DO NOT MODIFY.

#ifndef UNPACK13_H_
#define UNPACK13_H_


#include "unpack.h"


#define UNPACK_13BIT_MAX_COUNT 8

extern unpack_func_t unpack_13bit[UNPACK_13BIT_MAX_COUNT][8][2];

#endif /* UNPACK13_H_ */
