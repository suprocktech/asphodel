// AUTOGENERATED FILE. DO NOT MODIFY.

#ifndef UNPACK32_H_
#define UNPACK32_H_


#include "unpack.h"


#define UNPACK_32BIT_MAX_COUNT 32

extern unpack_func_t unpack_32bit[UNPACK_32BIT_MAX_COUNT][8][2];

#endif /* UNPACK32_H_ */
